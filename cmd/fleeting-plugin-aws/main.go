package main

import (
	"gitlab.com/gitlab-org/fleeting/fleeting/plugin"
	aws "gitlab.com/gitlab-org/fleeting/plugins/aws"
)

func main() {
	plugin.Main(&aws.InstanceGroup{}, aws.Version)
}
